<?php 
/**Template Name: Contacto */
get_header();?>

<div class="main-container contacto">

    <div class="hero-header" style="background-image: url('<?=get_field('imagen_header_contacto')['url']?>')">
        <div class="text-block">
            <?php the_field('texto_header_contacto');?>
        </div>
    </div>

    <div class="contacto-info-container">
        <div class="col col-1">
            <div class="text-heading-holder">
                <h2><?=get_field('texto_caja_contacto');?></h2>
            </div>
            <div class="address-holder">
                <?php the_field('direccion', 199);?>
                <?php
                    $rows = get_field('redes_sociales', 199); 
                    if($rows):?>
                    <div class="rrss-holder">
                        <?php foreach($rows as $row):?>
                            <a target="_blank" href="<?=$row['url']; ?> "><i class="fab fa-<?=$row['icono_fontawesome'];?>"></i></a> 
                        <?php endforeach;?>
                    </div>
                <?php endif;?>
            </div>
        </div>
        <div class="col col-2">
            <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');?>
        </div>
    </div>

</div>

<?php get_footer();?>