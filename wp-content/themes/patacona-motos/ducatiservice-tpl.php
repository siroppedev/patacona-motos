<?php 

/**Template Name: Ducati Service */
get_header();?>

<div class="main-container service">

    <div class="hero-header" style="background-image: url('<?=get_field('imagen_header_contacto')['url']?>')">
        <div class="text-block">
            <?php the_field('texto_header_contacto');?>
        </div>
    </div>

    <div class="contacto-info-container">
        <div class="col col-1">
            <div class="text-heading-holder">
                <h2><?=get_field('texto_caja_contacto');?></h2>
            </div>
            <div class="address-holder">
                <p>Puedes solicitar cita previa desde el formulario, o llamando al 960 453 900.</p>
            </div>
        </div>
        <div class="col col-2">
            <?php echo do_shortcode('[contact-form-7 id="1031" title="Ducati Service Form"]');?>
        </div>
    </div>

</div>

<?php get_footer();?>