<?php get_header();?>

<div class="main-container marca-archive">

    

    <?php 
       $term = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

       $img_hero = get_field('imagen_header_marca', $term);
       if(!$img_hero) {
            $img_hero = get_field('imagen_hero_blog', 22);
       } 
    ?>
    <div class="hero-header" style="background-image: url('<?=$img_hero['url']?>')">
        <div class="black-overlay"></div>
        <div class="text-block">
            <h2><?=$term->name;?></h2>
            <p>Conoce nuestras marcas</p>
            <div class="brand-description-container">
                <?php the_field('texto_descriptivo_marca', $term);?>
            </div>
        </div>
    </div>

    <div class="models-container">

       <?php 
            $args = array(
                'post_type' => 'modelos',
                'posts_per_page' => -1,
                'tax_query'  => array(
                    array(
                        'taxonomy' => 'marcas',
                        'field'    => 'slug',
                        'terms'    => $term
                    )
                )
            );
            
            $modelos = new WP_Query($args);
       ?>

        <?php while($modelos->have_posts()):$modelos->the_post();?>
            <a class="model" href="<?=get_the_permalink();?>">
                <div class="thumbnail-holder">
                    <img src="<?=get_the_post_thumbnail_url();?>" alt="">
                </div>
                <div class="info-container">
                    <?php the_title('<h2>', '</h2>');?>

                    <div class="button-container">
                        <p>Ver moto</p>
                    </div>
                </div>
            </a>
        <?php endwhile;?>
    </div>

</div>

<?php get_footer();?>