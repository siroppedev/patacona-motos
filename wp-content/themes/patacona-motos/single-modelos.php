<?php get_header();?>

<div class="main-container single-model">

    <?php $variaciones_colores = get_field('colores');?>

    <div class="model-info">
        <div class="col col-images">
            <div class="color-gallery-container">
                <?php 
                    $counter = 0; 
                    foreach($variaciones_colores as $variacion_color):?>
                    <div class="color-gallery owl-carousel">
                        <?php 
                            $imagenes_galeria_detalle_color = $variacion_color['galeria_detalle'];
                            foreach($imagenes_galeria_detalle_color as $imagen_detalle):
                            $img = $imagen_detalle['imagen_genereal'];
                        ?>
                            <img src="<?=$img['url']?>" alt="<?=$img['alt']?>">
                            <?php endforeach;?>  
                    </div>     
                <?php $counter++; endforeach;?>
            </div>
        </div>
        <div class="col col-info">
            <div class="main-product-info">
                
                <div class="product-name">

                    <?php the_title('<h1>', '</h1>');?>
                </div>
                <div class="model-price-holder">
                    <?php 
                        $counter = 0;
                        foreach($variaciones_colores as $variacion_color):
                            $price = (strpos($variacion_color['precio'], '€')) ? $variacion_color['precio'] : $variacion_color['precio'].'€';
                        ?>
                            <p class="color-price"><?=$price?></p>
                    <?php endforeach;?>    
                </div>
                <div class="colors">
                    <?php foreach ($variaciones_colores as $color) { ?>
                        <div class="box desactive" style="border-color: <?php echo $color['color_secundario']; ?>">
                            <div class="box-content" style="background-color: <?php echo $color['color_secundario']; ?>"></div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="container-property">

                <div class="row-property caracteristicas">
                    <div class="title-property">
                        <h3>Motor</h3>
                        <div class="plus-minus-toggle collapsed"></div>
                    </div>
                    <div class="dynamic-content">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('motor') ):

                        // loop through the rows of data
                            while ( have_rows('motor') ) : the_row();?>

                            <div class="row-value">
                            <p><?php the_sub_field('propiedad');?></p>
                            <p><?php the_sub_field('valor');?></p>
                            </div>

                        <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>


                <div class="row-property caracteristicas">
                    <div class="title-property">
                    <h3>Parte ciclo</h3>
                    <div class="plus-minus-toggle collapsed"></div>
                    </div>
                    <div class="dynamic-content">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('parte_ciclo') ):

                        // loop through the rows of data
                            while ( have_rows('parte_ciclo') ) : the_row();?>

                            <div class="row-value">
                            <p><?php the_sub_field('propiedad');?></p>
                            <p><?php the_sub_field('valor');?></p>
                            </div>

                        <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>

                <div class="row-property caracteristicas">
                    <div class="title-property">
                    <h3>Dimensiones y pesos</h3>
                    <div class="plus-minus-toggle collapsed"></div>
                    </div>
                    <div class="dynamic-content">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('dimensiones_y_pesos') ):

                        // loop through the rows of data
                            while ( have_rows('dimensiones_y_pesos') ) : the_row();?>

                            <div class="row-value">
                            <p><?php the_sub_field('propiedad');?></p>
                            <p><?php the_sub_field('valor');?></p>
                            </div>

                        <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>


                <div class="row-property caracteristicas">
                    <div class="title-property">
                    <h3>Transmisión</h3>
                    <div class="plus-minus-toggle collapsed"></div>
                    </div>
                    <div class="dynamic-content">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('transmision') ):

                        // loop through the rows of data
                            while ( have_rows('transmision') ) : the_row();?>

                            <div class="row-value">
                            <p><?php the_sub_field('propiedad');?></p>
                            <p><?php the_sub_field('valor');?></p>
                            </div>

                        <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>

                <div class="row-property caracteristicas">
                    <div class="title-property">
                    <h3>Instrumentos y componentes eléctricos</h3>
                    <div class="plus-minus-toggle collapsed"></div>
                    </div>
                    <div class="dynamic-content">
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('instrumentos_electricos') ):

                        // loop through the rows of data
                            while ( have_rows('instrumentos_electricos') ) : the_row();?>

                            <div class="row-value">
                            <p><?php the_sub_field('propiedad');?></p>
                            <p><?php the_sub_field('valor');?></p>
                            </div>

                        <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>

                <?php if(get_field('info_financiacion')):?>
                    <div class="row-property caracteristicas">
                        <div class="title-property">
                        <h3>Info. Financiación</h3>
                        <div class="plus-minus-toggle collapsed"></div>
                        </div>
                        <div class="dynamic-content">
                            <?php if(get_field('info_financiacion')):?>
                            
                                <?php the_field('info_financiacion');?>
                            
                            <?php endif;?>
                        </div>
                    </div>
                <?php endif;?>

                <?php if(get_field('disclaimer_legal')):?>
                    <div class="legal-disclaimer-holder">
                        <?php the_field('disclaimer_legal');?>
                    </div>
                <?php endif;?>

                <div class="contact-buttons-holder">
                    <div id="test-button" class="button">
                        <p>SOLICITA PRUEBA</p>
                    </div>

                    <div id="info-button" class="button">
                        <p>SOLICITA INFORMACIÓN</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_rows('galeria_general')):?>
        <div class="model-gallery">
            <div class="gallery owl-carousel">
                <?php while(have_rows('galeria_general')):the_row();?>
                    <?php $gallery_img = get_sub_field('imagen_galeria');?>
                    <div class="image-container">
                        <a class="img-wrap" href="<?php echo $gallery_img['url'] ?>">
                        <img src="<?=$gallery_img['url'];?>" alt="<?=$gallery_img['alt']?>">
                        </a>
                    </div>
                <?php endwhile;?>
            </div>
        </div>
    <?php endif;?>

    <div class="related-models">
        
        <div class="related-models-title">
            <h3>Otros modelos</h3>
        </div>

        <div class="related-models-gallery owl-carousel">
            <?php 
                $post_ID = get_the_ID();
                $post_brand = wp_get_post_terms($post_ID, 'marcas')[0]->slug;
                
                $args = array(
                    'post_type'         => 'modelos',
                    'posts_per_page'    => -1,
                    'post__not_in'      => array($post_ID),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'marcas',
                            'field' => 'slug',
                            'terms' => $post_brand,
                        )
                    )
                );

                $related_models = new WP_Query($args);
                while($related_models->have_posts()):$related_models->the_post();
            ?>
                <a class="model" href="<?=get_the_permalink();?>">
                    <div class="thumbnail-holder">
                        <img src="<?=get_the_post_thumbnail_url();?>" alt="">
                    </div>
                    <div class="info-container">
                        <?php the_title('<h2>', '</h2>');?>

                        <div class="button-container">
                            <p>Ver moto</p>
                        </div>
                    </div>
                </a>
            <?php endwhile;?>
        </div>
    </div>
</div>


<div class="transparencia"></div>

<div class="pop-up--form pop-up--prueba">
    <img src="<?php echo get_template_directory_uri(); ?>/img/cross.svg" class="cross" alt="">
    <?php echo do_shortcode('[contact-form-7 id="171" title="Solicitar Prueba"]');?>
</div>

<div class="pop-up--form pop-up--info">
    <img src="<?php echo get_template_directory_uri(); ?>/img/cross.svg" class="cross" alt="">
    <?php echo do_shortcode('[contact-form-7 id="172" title="Solicitar Informacion"]');?>
</div>


<?php get_footer();?>