

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="flex-holder">
            <div class="col col-izq">
                <div class="productos">
                    <div class="menu-title">
                        <h3>Nuestras marcas</h3>
                    </div>
                    <div class="brands-menu-holder">
                        <?php
                            $links_marcas = wp_get_nav_menu_items(3);
                            $counter = 0;
                            foreach($links_marcas as $link_marca):
                                if($counter % 4 != 0):
                                    echo "<li><a href='".$link_marca->url."'>".$link_marca->title."</a></li>";                                
                                else:
                                    echo "</ul>";
                                    echo "<ul class='col-menu'>";
                                    echo "<li><a href='".$link_marca->url."'>".$link_marca->title."</a></li>";
                                endif;    
                                $counter++;
                            endforeach;
                            echo "</ul>"
                        ?>
                    </div>
                </div>
            </div>
            <div class="col col-der">
                <div class="contacto">
                    <div class="menu-title">
                        <h3>Contacta con nosotros</h3>
                    </div>
                    <?php the_field('direccion', 199);?>
                </div>
                <div class="mas-info">
                    <div class="menu-title">
                        <h3>Más información</h3>
                    </div>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'info-menu',
                            'menu_id'        => 'info-menu',
                        ) );
                    ?>
                    <?php
                        $rows = get_field('redes_sociales', 199); 
                        if($rows):?>
                        <div class="rrss-holder">
                            <?php foreach($rows as $row):?>
                                <a target="_blank" href="<?=$row['url']; ?> "><i class="fab fa-<?=$row['icono_fontawesome'];?>"></i></a> 
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
		<div class="bottom-text">
            <p>@2018 Grupo Motorien. Todos los derechos reservados</p>
            <p>Hecho por <a href="https://siroppe.com">Siroppe</a></p>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
