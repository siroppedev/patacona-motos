<?php 
    /** Template Name: Vehiculos de Ocasion */
    get_header();
?>
<div class="main-container ocasion">
    
<div class="hero-header" style="background-image: url('<?=get_field('imagen_hero_ocasion')['url']?>')">
        <div class="text-block">
            <?php the_field('texto_hero_ocasion');?>
            <div class="buttons-holder">
                <a class="button form-button" href="#">
                    SOLICITA INFORMACIÓN
                </a>
                <a target="_blank" class="button" href="https://motos.coches.net/">
                    Ver motos.net
                </a>
            </div>
        </div>
        
    </div>

    <div class="models-container">
        <?php 
        $args = array(
            'post_type' => 'modelos',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'vehiculos_ocasion',
                    'field' => 'slug',
                    'terms' => 'es_ocasion',
                )
            )
        );
          
        $prods = new WP_Query($args);

        while($prods->have_posts()):$prods->the_post();?>
            <a class="model" target="_blank" href="<?=get_field('url_motosnet');?>">
                <div class="thumbnail-holder">
                    <img src="<?=get_the_post_thumbnail_url();?>" alt="">
                </div>
                <div class="info-container">
                    <?php the_title('<h2>', '</h2>');?>

                    <div class="button-container">
                        <p>Ver moto</p>
                    </div>
                </div>
            </a>
        <?php endwhile; wp_reset_postdata();?>
    </div>

</div>


<div class="transparencia"></div>

<div class="pop-up--form pop-up--ocasion">
    <img src="<?php echo get_template_directory_uri(); ?>/img/cross.svg" class="cross" alt="">
    <?php echo do_shortcode('[contact-form-7 id="995" title="Formulario Ocasion"]');?>
</div>

<?php get_footer();?>