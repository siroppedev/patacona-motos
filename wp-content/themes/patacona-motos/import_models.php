<?php
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php');


    $json_zontes_info = file_get_contents('https://zontesmotos.es/wp-content/themes/zontes-srp/json/models_export.json'); 
    $zontes_models_info = json_decode($json_zontes_info);

    
    $json_smw_info = file_get_contents('http://swm.com.es/wp-content/themes/swm-srp/json/models_export.json'); 
    $smw_models_info = json_decode($json_smw_info);

    upload_models($zontes_models_info, '11');
    upload_models($smw_models_info, '8');
    

    function upload_models($models_info, $tax_id) {
        foreach($models_info as $model_info):
            $args = array(
                'post_title' => $model_info->nombre_modelo,
                'post_type' => 'modelos',
                'post_status' => 'publish'
            );
        
            /*** Creating the new Post */
            $new_model = wp_insert_post($args);
            wp_set_post_terms($new_model, $tax_id, 'marcas');
        
            /** Setting new Post thumbnail */
            if(gettype($model_info->thumbnail_modelo) == "string"):
                $thumbnail_url = $model_info->thumbnail_modelo;
            else:
                $thumbnail_url = $model_info->thumbnail_modelo->url;
            endif;
            $thumb_attach_id = create_wp_attachment_from_url($thumbnail_url);
            set_post_thumbnail($new_model, $thumb_attach_id);
        
            /*** Adding product variants  */
            $variaciones_color = $model_info->variaciones_color;
        
            if($variaciones_color):
                foreach($variaciones_color as $variacion_color):
                    $color  = $variacion_color->color;
                    $precio = $variacion_color->precio_modelo;
                    $galeria = array();
        
                    $fotos_generales = $variacion_color->fotos_generales;
            
                    if($fotos_generales):
                        foreach($fotos_generales as $foto_general):
                            $foto_general_url = $foto_general->url;
                            $imagen_galeria_detalle = array(
                                'imagen_genereal' => create_wp_attachment_from_url($foto_general_url)
                            );
                            array_push($galeria, $imagen_galeria_detalle);
                        endforeach; 

                        $row = array(
                            'color_secundario'  => $color,
                            'precio'            => $precio,
                            'galeria_detalle'   => $galeria
                        );
                        add_row('colores', $row ,$new_model);
                    endif;    
                endforeach;
            endif;
            
        
        
            /*** Adding Product Information */
            $motor_values = $model_info->motor;
        
            if($motor_values):
                foreach($motor_values as $motor_value):
                    $row = array(
                        'propiedad' => $motor_value->propiedad,
                        'valor' => $motor_value->valor,
                    );
                    add_row('motor', $row ,$new_model);
                endforeach;
            endif;
        
            $parte_ciclo_values = $model_info->parte_ciclo;
        
            if($parte_ciclo_values):
                foreach($parte_ciclo_values as $parte_ciclo_value):
                    $row = array(
                        'propiedad' => $parte_ciclo_value->propiedad,
                        'valor' => $parte_ciclo_value->valor,
                    );
                    add_row('parte_ciclo', $row ,$new_model);
                endforeach;
            endif;
        
            $dimensiones_y_pesos_values = $model_info->dimensiones_y_pesos;
        
            if($dimensiones_y_pesos_values):
                foreach($dimensiones_y_pesos_values as $dimensiones_y_pesos_value):
                    $row = array(
                        'propiedad' => $dimensiones_y_pesos_value->propiedad,
                        'valor' => $dimensiones_y_pesos_value->valor,
                    );
                    add_row('dimensiones_y_pesos', $row ,$new_model);
                endforeach;
            endif;
        
            $transmision_values = $model_info->transmision;
        
            if($transmision_values):
                foreach($transmision_values as $transmision_value):
                    $row = array(
                        'propiedad' => $transmision_value->propiedad,
                        'valor' => $transmision_value->valor,
                    );
                    add_row('transmision', $row ,$new_model);
                endforeach;
            endif;
        
            $instrumentos_electricos_values = $model_info->instrumentos_electricos;
        
            if($instrumentos_electricos_values):
                foreach($instrumentos_electricos_values as $instrumentos_electricos_value):
                    $row = array(
                        'propiedad' => $instrumentos_electricos_value->propiedad,
                        'valor' => $instrumentos_electricos_value->valor,
                    );
                    add_row('instrumentos_electricos', $row ,$new_model);
                endforeach;
            endif;
        
        
            $fotos_detalle = $model_info->fotos_detalle;
            
            if($fotos_detalle):
                foreach($fotos_detalle as $foto_detalle):
                    $foto_detalle_url = $foto_detalle->url;
                    $attch_id = create_wp_attachment_from_url($foto_detalle_url);
                    $row = array(
                        'imagen_galeria' => $attch_id
                    );
                    add_row('galeria_general', $row ,$new_model);
                endforeach;
            endif;
        endforeach;
    }

    function create_wp_attachment_from_url($image_url) {
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($image_url);
        $filename = basename($image_url);
        $attach_id = '';
        $attach_args = array(
            'posts_per_page' => 1,
            'post_type' => 'attachment',
            'name' => sanitize_file_name($filename)
        );
    
        $attach_check = new WP_Query($attach_args);
    
        if($attach_check->have_posts()) {
            while($attach_check->have_posts()){
                $attach_check->the_post();
                $attach_id = get_the_ID();
            }
            wp_reset_postdata();
        } else {
            if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
            else                                    $file = $upload_dir['basedir'] . '/' . $filename;
            file_put_contents($file, $image_data);
    
            $wp_filetype = wp_check_filetype($filename, null );
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => sanitize_file_name($filename),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id = wp_insert_attachment( $attachment, $file, 0 );
        }
        return $attach_id;
    } 
?>

<!-- 
<pre>
    <?//print_r($smw_models_info);?>
</pre> -->
