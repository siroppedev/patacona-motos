$ = jQuery.noConflict();

$(document).ready(function() {
    

    var brandsDropdown = $('.menu-brands-holder').html();
    $('.menu-brands-holder').remove();
    $('.brands-dropdown.menu-item').append('<div class="menu-brands-holder">' + brandsDropdown + '</div>');


    if ( $(window).width() < 992 ) {
        $('body').on('click', '.brands-dropdown', function() {
            if (!$(this).hasClass('active')) {
                $(this).find('.menu-brands-holder').slideDown();
                $(this).addClass('active');
                $('#mobile-menu').addClass('taller');
            }
            else {
                $(this).find('.menu-brands-holder').slideUp('fast');
                $(this).removeClass('active');
                $('#mobile-menu').removeClass('taller');
            }

        });
    } else {
        $('body').on('mouseenter', '.brands-dropdown', function() {        
            if (!$(this).hasClass('active')) {
                $(this).find('.menu-brands-holder').slideDown();
                $(this).addClass('active');
            }
        });
        $('body').on('mouseleave', '.brands-dropdown', function() {      
            if ($(this).hasClass('active')) {
                $(this).find('.menu-brands-holder').slideUp('fast');
                $(this).removeClass('active');
            }
        });
    }

    $('#burger').on('click', function() {
        $('#burger').toggleClass('open');
        
        if($('body').hasClass('single-modelos')) {
            $('#masthead').toggleClass('black-menu');
        }

        $('.menu-mobile-container').toggleClass('open');

        if($('#burger').hasClass('open')) {
            $('body, html').css('overflow-y', 'hidden');
        } else {
            $('body, html').css('overflow-y', 'initial');
        }
    });


    if($('body').hasClass('page-template-home-tpl')) {
        $('.brands-container-mobile.owl-carousel').owlCarousel({
            margin: 20,
            dots: true,
            loop:false,
            nav: false,
            responsive:{
                0: {
                    items: 1,
                },
                700: {
                    items: 2,
                }
            }
        });
    }
    
    if($('body').hasClass('page-template-conocenos-tpl')) {
        $('.owl-carousel').owlCarousel({
            margin: 20,
            dots: true,
            loop:false,
            nav: false,
            responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });

        $(".gallery-holder").lightGallery({
            selector: '.owl-item .img-wrap'
        });
       
    }   

    if($('body').hasClass('page-template-ocasion-tpl')) {
        $('.buttons-holder .form-button').on('click', function(e) {
            e.preventDefault();
            $('.pop-up--ocasion').addClass('active');
            $('.transparencia').addClass('active');
        });

        $('.pop-up--form .cross').on('click', function() {
            $(this).parent().removeClass('active');
            $('.transparencia').removeClass('active');
        });

        $('#file-upload input[type="file"]').on('change', function(e){
            if(e.target.files.length > 0) {
                var fileName = e.target.files[0].name;
                fileName = fileName.substring(0, 17);
                $('.fake-file-upload p').text(fileName);
            } else {
                $('.fake-file-upload p').text('Subir currículum');
            }
        });

        $('.fake-file-upload').on('click', function(e) {
            e.preventDefault();
            $('#file-upload input[type="file"]').trigger('click');
        });
    }

    if($('body').hasClass('single-modelos')) {

        $('#masthead').addClass('black-menu');

        $('.color-gallery.owl-carousel').owlCarousel({
            loop:true,
            margin:0,
                  autoplay:false,
                  animateOut: 'fadeOut',
                  thumbs: true,
            thumbImage: true,
            dots: false,
            thumbContainerClass: 'owl-thumbs',
            thumbItemClass: 'owl-thumb-item',
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:1,
                    nav:false
                },
                1000:{
                    items:1,
                    nav:false,
                    loop:true
                }
            }
        });
        
        $('.gallery.owl-carousel').owlCarousel({
            margin: 20,
            dots: true,
            loop:false,
            nav: false,
            responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });

        $('.related-models-gallery.owl-carousel').owlCarousel({
            margin: 20,
            dots: true,
            loop:false,
            nav: false,
            responsive:{
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });


        $('.row-property').on('click', function(e) {
            $(this).find('.plus-minus-toggle').toggleClass('collapsed');
            $(this).find('.dynamic-content').toggleClass('open');
            if( $(this).find('.dynamic-content').hasClass('open')) {
                $(this).find('.dynamic-content').slideDown();
            } else {
                $(this).find('.dynamic-content').slideUp();
            }
        });

        $('.main-product-info .colors .box').on('click', function() {
            var index = $(this).index();

            $('.color-gallery-container .color-gallery').siblings().css('display', 'none');
            $('.main-product-info .colors .box').siblings().addClass('desactive');
            $('.model-price-holder .color-price').siblings().css('display', 'none');

            $(this).removeClass('desactive');
            $($('.color-gallery-container .color-gallery')[index]).css('display', 'block');
            $($('.model-price-holder .color-price')[index]).css('display', 'block');
            
        });


        $('.color-gallery-container .color-gallery:nth-of-type(1)').css('display', 'block');
        $('.main-product-info .colors .box:nth-of-type(1)').removeClass('desactive');
        $('.model-price-holder .color-price:nth-of-type(1)').css('display', 'block');

        $titulo = $('.product-name h1').text();
        $('.pop-up--prueba form').find('input[name="model"]').val($titulo);
        $('.pop-up--info form').find('input[name="model"]').val($titulo);

        $('.contact-buttons-holder #test-button').on('click', function() {
            $('.pop-up--prueba').addClass('active');
            $('.pop-up--info').removeClass('active');
            $('.transparencia').addClass('active');
        });
        
        $('.contact-buttons-holder #info-button').on('click', function() {
            $('.pop-up--prueba').removeClass('active');
            $('.pop-up--info').addClass('active');
            $('.transparencia').addClass('active');
        });

        $('.pop-up--form .cross').on('click', function() {
            $(this).parent().removeClass('active');
            $('.transparencia').removeClass('active');
        });

        $('.transparencia').on('click', function() {
            $('.pop-up--info').removeClass('active');
            $('.pop-up--prueba').removeClass('active');
            $(this).removeClass('active');
        });

        $(".model-gallery").lightGallery({
            selector: '.owl-item .img-wrap'
        });
    }
});