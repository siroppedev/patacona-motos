<?php 
/** Template Name: Home */
get_header();?>

<div class="main-container home">
    <div class="hero-header" style="background-image: url('<?=get_field('imagen_de_fondo_header')['url']?>')">
        <div class="text-block">
            <?php the_field('texto_header');?>
            <div class="cta-button-holder">
                <a href="<?=get_field('link_cta')['url']?>"><?=get_field('link_cta')['title']?></a>
            </div>
        </div>
    </div>

    <div class="our-brands">

        <div class="intro-text">
            <h2>Conoce nuestras marcas</h2>
        </div>
        
        <div class="brands-container-desktop">
            <?php
                $brands = get_terms(
                    array(
                        'taxonomy' => 'marcas',
                        'hide_empty' => false
                    )
                );
                foreach($brands as $brand): 
            ?>
                <div class="brand-box" style="background-image:url('<?=get_field('imagen_de_fondo', $brand)['url'];?>')">
                    <div class="logo-holder">
                        <img src="<?=get_field('logo', $brand)['url'];?>" alt="<?=$brand->slug?>-logo">
                    </div>

                    <div class="link-holder">
                        <a href="<?=get_term_link($brand, 'marcas')?>">ver motos</a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>

        <div class="brands-container-mobile owl-carousel">
            <?php
                $brands = get_terms(
                    array(
                        'taxonomy' => 'marcas',
                        'hide_empty' => false
                    )
                );
                foreach($brands as $brand): 
            ?>
                <div class="brand-box" style="background-image:url('<?=get_field('imagen_de_fondo', $brand)['url'];?>')">
                    <div class="logo-holder">
                        <img src="<?=get_field('logo', $brand)['url'];?>" alt="<?=$brand->slug?>-logo">
                    </div>

                    <div class="link-holder">
                        <a href="<?=get_term_link($brand, 'marcas')?>">ver motos</a>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>

    <div class="cta-boxes">
        <?php while(have_rows('contenedores_ctas')):the_row();?>
            <div class="cta-box">
                <div class="image-holder">
                    <?php $img = get_sub_field('imagen');?>
                    <img src="<?=$img['url']?>" alt="<?=$img['alt']?>">
                </div>
                <div class="text-holder">
                    <div class="text-container">
                        <?php the_sub_field('texto_cta');?>

                        <div class="link-holder">
                            <a href="<?=get_sub_field('link_cta')['url']?>"><?=get_sub_field('link_cta')['title']?></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile;?>
    </div>
</div>



<?php get_footer();?>