<?php get_header();?>

<div class="main-container blog">
    <div class="hero-header" style="background-image: url('<?=get_field('imagen_hero_blog', 22)['url']?>')">
        <div class="text-block">
            <?php the_field('texto_hero_blog', 22);?>
        </div>
    </div>

    <div class="news-holder">
        <?php 
            
            while(have_posts()):the_post();
        ?>

            <a class="post" href="<?=get_the_permalink();?>">
                <div class="post-thumbnail" style="background-image:url('<?=get_the_post_thumbnail_url()?>')"></div>
                <div class="post-excerpt-holder">
                    <div class="post-title">
                        <?php the_title('<h3>', '</h3>');?>
                    </div>
                    <div class="excerpt">
                        <?php the_excerpt();?>
                    </div>
                    <img class="arrow" src="<?=get_template_directory_uri()?>/img/arrow.svg" alt="">
                    
                </div>
            </a>


        <?php endwhile;?>
    </div>
</div>

<?php get_footer();?>