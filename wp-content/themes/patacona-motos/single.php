<?php
get_header();
?>

	<div class="main-container blog-post">
        <div class="hero-header" style="background-image: url('<?=get_the_post_thumbnail_url();?>')">
            <div class="post-info-holder">
                <div class="title-holder">
                    <?php the_title('<h1>', '</h1>');?>
                </div>
                <div class="date-holder">
                    <?php the_date('d/m/y');?>
                </div>
            </div>
        </div>


        <div class="post-content">
            <?php while(have_posts()):the_post(); the_content(); endwhile;?>
        </div>

        <div class="cont-nav">
            <div class="navegacion">
                <?php $prev = get_permalink(get_adjacent_post(false,'',true));?>
                <?php $next = get_permalink(get_adjacent_post(false,'',false));?>
                <a class="anterior" href="<?php echo $prev?>">
                    <div class="nav-bttn">
                        <p>Noticia anterior</p>
                    </div>
                </a> 
                <a class="siguiente" href="<?echo $next?>">
                    <div class="nav-bttn">
                        <p>Noticia siguiente</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php get_footer();?>