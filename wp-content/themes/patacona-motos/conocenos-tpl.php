<?php 
/** Template Name: Conocenos */
get_header();?>

<div class="main-container conocenos">

    <div class="hero-header" style="background-image: url('<?=get_field('imagen_de_fondo')['url']?>')">
        <div class="text-block">
            <?php the_field('texto');?>
        </div>
    </div>

    <div class="main-text-container">
        <div class="text-heading-holder">
            <h2><?=get_field('encabezado');?></h2>
        </div>

        <div class="text-columns-holder">
            <div class="text-col">
                <?php the_field('columna_1');?>
            </div>
            <div class="text-col">
                <?php the_field('columna_2');?>
            </div>
        </div>

    </div>


    <div class="galery-container">
        <div class="gallery-holder owl-carousel">
            <?php while(have_rows('galeria_conocenos')):the_row();?>
                <?php $img = get_sub_field('imagen');?>
                <div class="image-container">
                    <a class="img-wrap" href="<?php echo $img['url'] ?>">
                        <img src="<?=$img['url'];?>" alt="">
                    </a>
                </div>
            <?php endwhile;?>
        </div>
    </div>

</div>

<?php get_footer();?>